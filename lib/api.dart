import 'dart:io';

import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

String baseUrl = 'https://app.ayo.moph.go.th';
// String baseUrl = 'https://1de161ba3685.ngrok.io';

Dio dio = new Dio(new BaseOptions(
    baseUrl: baseUrl,
    receiveDataWhenStatusError: true,
    connectTimeout: 60 * 1000,
    receiveTimeout: 60 * 1000));

class Api {
  String imageUrl = baseUrl;

  Api() {
    dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      compact: true,
    ));
  }

  Future<Response> login(String username, String password) async {
    String path = '/login';

    return await dio
        .post(path, data: {"username": username, "password": password});
  }

  Future<Response> search(String token) async {
    String path = '/services/search';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> service(String hospcode, String hn, String token) async {
    String path = '/services/service?hospcode=$hospcode&hn=$hn';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> opdScreen(
      String hospcode, String hn, String vn, String token) async {
    String path = '/services/opdscreen?hospcode=$hospcode&hn=$hn&vn=$vn';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> drug(
      String hospcode, String hn, String vn, String token) async {
    String path = '/services/drug?hospcode=$hospcode&hn=$hn&vn=$vn';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> diagnosis(
      String hospcode, String hn, String vn, String token) async {
    String path = '/services/diagnosis?hospcode=$hospcode&hn=$hn&vn=$vn';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> iot(String token) async {
    String path = '/services/iot';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> getInfo(String token) async {
    String path = '/services/user/info';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> updateInfo(String firstName, String lastName, String email,
      String mobilePhone, String token) async {
    String path = '/services/user/info';
    return await dio.put(path,
        data: {
          "firstname": firstName,
          "lastname": lastName,
          "email": email,
          "mobile_phone": mobilePhone,
        },
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> getHealthInfo(String token) async {
    String path = '/services/health';
    return await dio.get(path,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> updateHealthInfo(
      int height, int weight, double waist, String token) async {
    String path = '/services/health';
    return await dio.put(path,
        data: {
          "waist": waist,
          "height": height,
          "weight": weight,
        },
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }

  Future<Response> uploadImage(File imageFile, String token) async {
    String path = '/services/info/upload';

    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(imageFile.path,
          filename: basename(imageFile.path))
    });

    return await dio.put(path,
        data: formData,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
  }
}
