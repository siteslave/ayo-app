import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final _formKey = GlobalKey<FormState>();

  final storage = new FlutterSecureStorage();

  Api api = Api();

  TextEditingController ctrlFirstname = TextEditingController();
  TextEditingController ctrlLastname = TextEditingController();
  TextEditingController ctrlEmail = TextEditingController();
  TextEditingController ctrlMobilePhone = TextEditingController();

  Future getInfo() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      var res = await api.getInfo(_token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        if (res.data["results"] != null) {
          var info = res.data["results"];

          setState(() {
            ctrlFirstname.text = info["firstname"];
            ctrlLastname.text = info["lastname"];
            ctrlEmail.text = info["email"];
            ctrlMobilePhone.text = info["mobile_phone"];
          });
        }
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  Future saveInfo() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      String firstname = ctrlFirstname.text;
      String lastname = ctrlLastname.text;
      String email = ctrlEmail.text;
      String mobilePhone = ctrlMobilePhone.text;

      var res =
          await api.updateInfo(firstname, lastname, email, mobilePhone, _token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        Navigator.of(context).pop(true);
      } else {
        EasyLoading.showError("เกิดข้อผิดพลาด");
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  @override
  void initState() {
    super.initState();
    getInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูล'),
      ),
      body: ListView(
        children: [
          Container(
            decoration: BoxDecoration(color: Colors.white),
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'ระบุข้อมูลส่วนตัว',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    TextFormField(
                      controller: ctrlFirstname,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาระบุชื่อ';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.person),
                          labelText: 'ชื่อ',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาระบุสกุล';
                        }
                        return null;
                      },
                      controller: ctrlLastname,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.vpn_key),
                          labelText: 'สกุล',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาระบุอีเมล์';
                        }
                        return null;
                      },
                      controller: ctrlEmail,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.email),
                          labelText: 'อีเมล์',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาระบุเบอร์โทร';
                        }
                        return null;
                      },
                      controller: ctrlMobilePhone,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.phone),
                          labelText: 'เบอร์โทรศัพท์',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(20), primary: Colors.indigo),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          saveInfo();
                        }
                      },
                      icon: Icon(Icons.save),
                      label: Text('บันทึกข้อมูล')),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
