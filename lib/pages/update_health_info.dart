import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api.dart';

class UpdateHealthInfoPage extends StatefulWidget {
  @override
  _UpdateHealthInfoPageState createState() => _UpdateHealthInfoPageState();
}

class _UpdateHealthInfoPageState extends State<UpdateHealthInfoPage> {
  final _formKey = GlobalKey<FormState>();

  final storage = new FlutterSecureStorage();

  Api api = Api();

  TextEditingController ctrlWaist = TextEditingController();
  TextEditingController ctrlHeight = TextEditingController();
  TextEditingController ctrlWeight = TextEditingController();

  Future getHealthInfo() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      var res = await api.getHealthInfo(_token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        if (res.data["results"] != null) {
          var info = res.data["results"];

          setState(() {
            ctrlWeight.text = info["weight"];
            ctrlHeight.text = info["height"];
            ctrlWaist.text = info["waist"];
          });
        }
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  Future saveHealthInfo() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      double waist = double.tryParse(ctrlWaist.text) ?? 0;
      int height = int.tryParse(ctrlHeight.text) ?? 0;
      int weight = int.tryParse(ctrlWeight.text) ?? 0;

      var res = await api.updateHealthInfo(height, weight, waist, _token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        Navigator.of(context).pop(true);
      } else {
        EasyLoading.showError("เกิดข้อผิดพลาด");
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  @override
  void initState() {
    super.initState();
    getHealthInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูลสุขภาพ'),
      ),
      body: ListView(
        children: [
          Container(
            decoration: BoxDecoration(color: Colors.white),
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        'ระบุข้อมูลสุขภาพ',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    TextFormField(
                      controller: ctrlWaist,
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'ระบุรอบเอว';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.person),
                          labelText: 'รอบเอว',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาระบุส่วนสูง';
                        }
                        return null;
                      },
                      controller: ctrlHeight,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.vpn_key),
                          labelText: 'ส่วนสูง',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาระบุน้ำหนัก';
                        }
                        return null;
                      },
                      controller: ctrlWeight,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.grey[100],
                          prefixIcon: Icon(Icons.email),
                          labelText: 'น้ำหนัก',
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(20), primary: Colors.indigo),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          saveHealthInfo();
                        }
                      },
                      icon: Icon(Icons.save),
                      label: Text('บันทึกข้อมูล')),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
