import 'package:ayo_app/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'home.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController ctrlUsername = TextEditingController();
  TextEditingController ctrlPassword = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Api api = Api();

  Future login() async {
    EasyLoading.show(status: "รอซักครู่...");
    final storage = new FlutterSecureStorage();

    try {
      String username = ctrlUsername.text;
      String password = ctrlPassword.text;

      var res = await api.login(username, password);

      EasyLoading.dismiss();

      if (res.statusCode == 200) {
        if (res.data["ok"]) {
          String token = res.data["accessToken"];
          await storage.write(key: "token", value: token);
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          EasyLoading.showError("ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง");
          print(res.data["error"]);
        }
      } else {
        EasyLoading.showError("เกิดข้อผิดพลาด");
        print(res.data);
        print('เกิดข้อผิดพลาด');
      }
    } catch (error) {
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          SizedBox(
            height: 30,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Image.asset(
                  "assets/images/logo_MOPH.png",
                  width: 150,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "AYO SELF CARE",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF166638)),
              ),
              Text(
                "version 0.0.0",
                style: TextStyle(color: Colors.grey),
              ),
              Container(
                margin: EdgeInsets.all(40),
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: ctrlUsername,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'กรุณาระบุชื่อผู้ใช้งาน';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10),
                              border: InputBorder.none,
                              filled: true,
                              fillColor: Colors.grey[100],
                              prefixIcon: Icon(Icons.person),
                              labelText: 'ชื่อผู้ใช้งาน',
                              labelStyle: TextStyle(color: Colors.grey)),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'กรุณาระบุรหัสผ่าน';
                            }
                            return null;
                          },
                          controller: ctrlPassword,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              filled: true,
                              fillColor: Colors.grey[100],
                              prefixIcon: Icon(Icons.vpn_key),
                              labelText: 'รหัสผ่าน',
                              labelStyle: TextStyle(color: Colors.grey)),
                        ),
                      ],
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40, right: 40),
                child: Row(
                  children: [
                    Expanded(
                      child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(20),
                              primary: Color(0xFF166638)),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              login();
                            }
                          },
                          icon: Icon(Icons.admin_panel_settings),
                          label: Text('เข้าสู่ระบบ')),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
