import 'dart:io';
import 'dart:math';

import 'package:ayo_app/api.dart';
import 'package:ayo_app/pages/edit_profile.dart';
import 'package:ayo_app/pages/login.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final storage = new FlutterSecureStorage();

  File _image;
  final picker = ImagePicker();

  Api api = Api();
  String token;
  int rand;

  String fullname;
  String email;
  String mobilePhone;

  Future getInfo() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      var _rnd = new Random();

      setState(() {
        token = _token;
        rand = _rnd.nextInt(10000);
      });

      var res = await api.getInfo(_token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        if (res.data["results"] != null) {
          var info = res.data["results"];

          setState(() {
            fullname = '${info["firstname"] ?? '-'} ${info["lastname"] ?? '-'}';
            email = info["email"] ?? '-';
            mobilePhone = info["mobile_phone"] ?? '-';
          });
        }
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  Future uploadImage(File _imageFile) async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      var res = await api.uploadImage(_imageFile, _token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        var _rnd = new Random();
        setState(() {
          rand = _rnd.nextInt(10000);
        });
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 890,
        maxWidth: 890,
        imageQuality: 70);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);

        uploadImage(_image);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff3f3f4),
      appBar: AppBar(
        title: Text('ข้อมูลส่วนตัว'),
        actions: [
          IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () async {
                await storage.deleteAll();
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => LoginPage()));
              })
        ],
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          CachedNetworkImage(
            httpHeaders: {"Authorization": "Bearer $token"},
            maxHeightDiskCache: 200,
            imageUrl: '${api.imageUrl}/services/info/image?rnd=$rand',
            imageBuilder: (context, imageProvider) => Container(
              alignment: Alignment.center,
              child: Stack(
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    child: GestureDetector(
                      onTap: () async {
                        if (await Permission.camera.request().isGranted) {
                          getImage();
                        }
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey,
                          size: 30,
                        ),
                      ),
                    ),
                    bottom: 10,
                    right: 10,
                  ),
                ],
              ),
            ),
            placeholder: (context, url) =>
                Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Container(
              alignment: Alignment.center,
              child: Stack(
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://randomuser.me/api/portraits/men/85.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    child: GestureDetector(
                      onTap: () async {
                        if (await Permission.camera.request().isGranted) {
                          getImage();
                        }
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey,
                          size: 30,
                        ),
                      ),
                    ),
                    bottom: 10,
                    right: 10,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(color: Colors.white),
            child: ListTile(
              title: Text('ชื่อ'),
              trailing: Text(
                '$fullname',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.white),
            child: ListTile(
              title: Text('อีเมล์'),
              trailing:
                  Text('$email', style: TextStyle(fontWeight: FontWeight.bold)),
            ),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.white),
            child: ListTile(
              title: Text('เบอร์โทร'),
              trailing: Text('$mobilePhone',
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: ElevatedButton.icon(
                onPressed: () async {
                  var res = await Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditProfilePage()));
                  if (res != null) {
                    getInfo();
                  }
                },
                icon: Icon(Icons.edit),
                label: Text('แก้ไขข้อมูล')),
          )
        ],
      ),
    );
  }
}
