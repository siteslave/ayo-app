import 'package:ayo_app/widgets/widget_diagnosis.dart';
import 'package:ayo_app/widgets/widget_drug.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api.dart';
import '../helper.dart';

class EmrPage extends StatefulWidget {
  final String hospcode;
  final String hn;
  final String vn;

  EmrPage({@required this.hospcode, @required this.hn, @required this.vn});

  @override
  _EmrPageState createState() => _EmrPageState();
}

class _EmrPageState extends State<EmrPage> {
  Api api = Api();
  Helper helper = Helper();

  Map screen = {};

  final storage = new FlutterSecureStorage();

  Future getOpdScreen() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res =
          await api.opdScreen(widget.hospcode, widget.hn, widget.vn, token);
      EasyLoading.dismiss();

      if (res.data["ok"]) {
        if (res.data["results"].length != 0) {
          setState(() {
            screen = res.data["results"][0];
          });
        }
      }
    } catch (error) {
      EasyLoading.dismiss();
      print(error);
    }
  }

  @override
  void initState() {
    super.initState();
    getOpdScreen();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('ข้อมูลการให้บริการ'),
          bottom: TabBar(
            indicatorWeight: 3,
            // isScrollable: true,
            tabs: [
              Tab(
                icon: Icon(Icons.directions_car),
                text: 'VitalSign',
              ),
              Tab(icon: Icon(Icons.directions_transit), text: 'Diagnosis'),
              Tab(icon: Icon(Icons.directions_bike), text: 'Drug'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            ListView(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.blue[500],
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text(
                            'BMI',
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            '${screen["bmi"] ?? "-"}',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Text('HEIGHT', style: TextStyle(color: Colors.white)),
                          Text('${screen["height"]} cm',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20))
                        ],
                      ),
                      Column(
                        children: [
                          Text('WEIGHT', style: TextStyle(color: Colors.white)),
                          Text('${screen["weight"]} kg',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20))
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.pink[400],
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text(
                            'BP',
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            '${screen["bps"] ?? "-"}/${screen["bpd"] ?? "-"}',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Text('PULSE', style: TextStyle(color: Colors.white)),
                          Text('${screen["pulse_rate"]}',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20))
                        ],
                      ),
                      Column(
                        children: [
                          Text('TEMP', style: TextStyle(color: Colors.white)),
                          Text('${screen["temperature"]}',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20))
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            WidgetDiagnosis(
              hospcode: widget.hospcode,
              hn: widget.hn,
              vn: widget.vn,
            ),
            WidgetDrug(
              hospcode: widget.hospcode,
              hn: widget.hn,
              vn: widget.vn,
            ),
          ],
        ),
      ),
    );
  }
}
