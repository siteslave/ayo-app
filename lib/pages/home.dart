import 'dart:math';

import 'package:ayo_app/helper.dart';
import 'package:ayo_app/pages/hospitals.dart';
import 'package:ayo_app/pages/profile.dart';
import 'package:ayo_app/pages/update_health_info.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final storage = new FlutterSecureStorage();
  Api api = Api();
  Helper helper = Helper();

  List items = [];

  int rand;

  String fullname = '';
  String token;

  double waist = 0;
  int height = 0;
  int weight = 0;

  Future getIot() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res = await api.iot(token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        setState(() {
          items = res.data["results"];
        });
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  Future getHealth() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res = await api.getHealthInfo(token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        var data = res.data["results"];
        setState(() {
          waist = double.tryParse(data["waist"]) ?? 0;
          height = int.tryParse(data["height"]) ?? 0;
          weight = int.tryParse(data["weight"]) ?? 0;
        });
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  Future getInfo() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String _token = await storage.read(key: "token");

      var _rnd = new Random();

      setState(() {
        token = _token;
        rand = _rnd.nextInt(10000);
      });

      var res = await api.getInfo(_token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        if (res.data["results"] != null) {
          var info = res.data["results"];

          setState(() {
            fullname = '${info["firstname"] ?? '-'} ${info["lastname"] ?? '-'}';
          });
        }
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  @override
  void initState() {
    super.initState();
    getIot();
    getInfo();
    getHealth();
  }

  @override
  Widget build(BuildContext context) {
    List<Container> _buildWidget = items.map((e) {
      DateTime date = DateTime.tryParse(e["create_date"]) ?? DateTime.now();

      return Container(
        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: ListTile(
          // leading: CircleAvatar(
          //   // radius: 25,
          //   child: Icon(
          //     Icons.person,
          //     // size: 40,
          //     color: Colors.teal[400],
          //   ),
          //   backgroundColor: Colors.grey[100],
          // ),
          title: Text(
            'โรงพยาบาลอยุธยา',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('ความดัน ${e["bps"]}/${e["bps"]} มม.ปรอท'),
              Text('Pulse: ${e["pulse"]}'),
              Divider(),
              Row(
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.calendar_today,
                        size: 15,
                        color: Colors.blue,
                      ),
                      Text(' ${helper.toThaiDate(date)}',
                          style: TextStyle(color: Colors.grey))
                    ],
                  ),
                  SizedBox(width: 10),
                  Row(
                    children: [
                      Icon(Icons.timelapse, size: 15, color: Colors.blue),
                      Text('${helper.toTime(date)} น.',
                          style: TextStyle(color: Colors.grey))
                    ],
                  ),
                ],
              ),
            ],
          ),
          // trailing: Icon(Icons.keyboard_arrow_right),
        ),
      );
    }).toList();

    return Scaffold(
      backgroundColor: Color(0xfff3f3f4),
      body: ListView(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'สวัสดี,',
                    ),
                    Text('$fullname',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18))
                  ],
                ),
                GestureDetector(
                  onTap: () async {
                    await Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => ProfilePage()));

                    getInfo();
                    getHealth();
                  },
                  child: CachedNetworkImage(
                    httpHeaders: {"Authorization": "Bearer $token"},
                    maxHeightDiskCache: 200,
                    imageUrl: '${api.imageUrl}/services/info/image?rnd=$rand',
                    imageBuilder: (context, imageProvider) => Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: Colors.red,
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, left: 10, right: 10),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.blue[500],
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text(
                      'รอบเอว',
                      style: TextStyle(color: Colors.white),
                    ),
                    Text(
                      '$waist',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    )
                  ],
                ),
                Column(
                  children: [
                    Text('สูง', style: TextStyle(color: Colors.white)),
                    Text('$height cm',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20))
                  ],
                ),
                Column(
                  children: [
                    Text('นำ้หนัก', style: TextStyle(color: Colors.white)),
                    Text('$weight kg',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20))
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              'บริการ',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  children: [
                    Image.asset(
                      "assets/images/fatigue.png",
                      width: 40,
                    ),
                    Text('ข้อมูลส่วนตัว'),
                  ],
                ),
                height: 85,
                width: 105,
                margin: EdgeInsets.only(left: 10, right: 5),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => HospitalPage()));
                },
                child: Container(
                  child: Column(
                    children: [
                      Image.asset(
                        "assets/images/hospital.png",
                        width: 40,
                      ),
                      Text('ประวัติรักษา'),
                    ],
                  ),
                  height: 85,
                  width: 105,
                  margin: EdgeInsets.only(left: 5, right: 5),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  var res = await Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => UpdateHealthInfoPage()));
                  if (res != null) {
                    getHealth();
                  }
                },
                child: Container(
                  child: Column(
                    children: [
                      Image.asset(
                        "assets/images/stethoscope.png",
                        width: 40,
                      ),
                      Text('บันทึก'),
                    ],
                  ),
                  height: 85,
                  width: 105,
                  margin: EdgeInsets.only(left: 5, right: 10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'ประวัติตรวจสุขภาพ',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                GestureDetector(
                  onTap: () {
                    getIot();
                  },
                  child: Text(
                    'refresh',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.blue),
                  ),
                ),
              ],
            ),
          ),
          items.length > 0
              ? Column(
                  children: _buildWidget,
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.restore,
                      color: Colors.grey,
                      size: 30,
                    ),
                    Text(
                      'ไม่พบรายการ',
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
        ],
      ),
    );
  }
}
