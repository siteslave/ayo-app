import 'package:ayo_app/api.dart';
import 'package:ayo_app/pages/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class HospitalPage extends StatefulWidget {
  @override
  _HospitalPageState createState() => _HospitalPageState();
}

class _HospitalPageState extends State<HospitalPage> {
  Api api = Api();
  List items = [];

  final storage = new FlutterSecureStorage();

  Future getHospital() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res = await api.search(token);
      EasyLoading.dismiss();

      if (res.data["ok"]) {
        setState(() {
          items = res.data["results"];
        });
      }
    } catch (error) {
      EasyLoading.dismiss();
      print(error);
    }
  }

  @override
  void initState() {
    super.initState();
    getHospital();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff3f3f4),
      appBar: AppBar(
        title: Text('เลือกโรงพยาบาล'),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          var item = items[index];
          return Container(
            margin: EdgeInsets.only(left: 10, right: 10, top: 10),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: ListTile(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ServicesPage(
                        hospcode: item["gw_hospcode"], hn: item["hn"])));
              },
              isThreeLine: true,
              title: Text(
                '${item["gw_hospcode"]} - ${item["hospitalname"]}',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('HN: ${item["hn"]}'),
                  Text(
                      '${item["title_name"]}${item["first_name"]} ${item["last_name"]}')
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          );
        },
        itemCount: items.length ?? 0,
      ),
    );
  }
}
