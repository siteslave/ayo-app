import 'package:ayo_app/helper.dart';
import 'package:ayo_app/pages/emr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api.dart';

class ServicesPage extends StatefulWidget {
  final String hospcode;
  final String hn;

  ServicesPage({@required this.hospcode, @required this.hn});

  @override
  _ServicesPageState createState() => _ServicesPageState();
}

class _ServicesPageState extends State<ServicesPage> {
  Api api = Api();
  Helper helper = Helper();

  List items = [];

  final storage = new FlutterSecureStorage();

  Future getServices() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res = await api.service(widget.hospcode, widget.hn, token);
      EasyLoading.dismiss();

      if (res.data["ok"]) {
        setState(() {
          items = res.data["results"];
        });
      }
    } catch (error) {
      EasyLoading.dismiss();
      print(error);
    }
  }

  @override
  void initState() {
    super.initState();
    getServices();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff3f3f4),
      appBar: AppBar(
        title: Text('เลือกวันที่รับบริการ'),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          var item = items[index];
          DateTime visitDate =
              DateTime.tryParse(item["visit_date"]) ?? DateTime.now();

          return Container(
            margin: EdgeInsets.only(left: 10, right: 10, top: 10),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: ListTile(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EmrPage(
                          vn: item["vn"],
                          hn: item["hn"],
                          hospcode: item["gw_hospcode"],
                        )));
              },
              isThreeLine: true,
              title: Text(
                'วันที่ ${helper.toThaiDate(visitDate)} เวลา ${item["visit_time"]}',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('สิทธิ์: ${item["pttype"] ?? "-"}'),
                  Text('เลขที่ ${item["pttype_no"] ?? "-"}')
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          );
        },
        itemCount: items.length ?? 0,
      ),
    );
  }
}
