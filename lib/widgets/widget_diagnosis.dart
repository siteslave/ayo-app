import 'package:ayo_app/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class WidgetDiagnosis extends StatefulWidget {
  final String hospcode;
  final String hn;
  final String vn;

  WidgetDiagnosis(
      {@required this.hospcode, @required this.hn, @required this.vn});

  @override
  _WidgetDiagnosisState createState() => _WidgetDiagnosisState();
}

class _WidgetDiagnosisState extends State<WidgetDiagnosis> {
  final storage = new FlutterSecureStorage();
  Api api = Api();
  List items = [];

  Future getDiagnosis() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res =
          await api.diagnosis(widget.hospcode, widget.hn, widget.vn, token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        setState(() {
          items = res.data["results"];
        });
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  @override
  void initState() {
    super.initState();
    getDiagnosis();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        var item = items[index];
        var diagType = item["diagnosis_type"].toString() == "1"
            ? "Principle Diagnosis"
            : item["diagnosis_type"].toString() == "2"
                ? "xxx"
                : "yyy";
        return Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: BoxDecoration(color: Colors.white),
          child: ListTile(
            title: Text('${item["diagnosis_code"]}'),
            subtitle: Text('$diagType'),
          ),
        );
      },
      itemCount: items.length ?? 0,
    );
  }
}
