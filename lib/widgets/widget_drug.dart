import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../api.dart';

class WidgetDrug extends StatefulWidget {
  final String hospcode;
  final String hn;
  final String vn;

  WidgetDrug({@required this.hospcode, @required this.hn, @required this.vn});

  @override
  _WidgetDrugState createState() => _WidgetDrugState();
}

class _WidgetDrugState extends State<WidgetDrug> {
  final storage = new FlutterSecureStorage();
  Api api = Api();
  List items = [];

  Future getDrug() async {
    EasyLoading.show(status: "รอซักครู่...");
    try {
      String token = await storage.read(key: "token");
      var res = await api.drug(widget.hospcode, widget.hn, widget.vn, token);
      EasyLoading.dismiss();
      if (res.data["ok"]) {
        setState(() {
          items = res.data["results"];
        });
      }
    } catch (error) {
      print(error);
      EasyLoading.dismiss();
      EasyLoading.showError("เกิดข้อผิดพลาด");
    }
  }

  @override
  void initState() {
    super.initState();
    getDrug();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        var item = items[index];
        return Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: BoxDecoration(color: Colors.white),
          child: ListTile(
            title: Text('${item["drug_name"]}'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${item["usage1"]}'),
                Text('${item["usage2"]}'),
                Text('${item["usage3"]}'),
              ],
            ),
            trailing: Text('${item["qty"]}'),
          ),
        );
      },
      itemCount: items.length ?? 0,
    );
  }
}
