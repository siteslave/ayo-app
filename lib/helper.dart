import 'package:intl/intl.dart';

class Helper {
  Helper();

  String toThaiDate(DateTime date) {
    // create format
    var strDate = new DateFormat.MMMd('th_TH').format(date);
    var _strDate = '$strDate ${date.year + 543}';
    // return thai date
    return _strDate;
  }

  String toLongThaiDate(DateTime date) {
    // create format
    var strDate = new DateFormat.MMMMd('th_TH').format(date);
    var _strDate = '$strDate ${date.year + 543}';
    // return thai date
    return _strDate;
  }

  String toLongThaiDateAndTime(DateTime date) {
    // create format
    var strDate = new DateFormat.MMMMd('th_TH').format(date);
    var strTime = new DateFormat.Hm().format(date);
    var _strDate = '$strDate ${date.year + 543} $strTime';
    // return thai date
    return _strDate;
  }

  String toTime(DateTime date) {
    var strTime = new DateFormat.Hm().format(date);
    return strTime;
  }
}
